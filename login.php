<?php 
include'config/db.php';
include'config/functions.php';
include'config/myfunction.php';

if(isset($_SESSION['login_admin']) == 'login_admin')
{
    header("location: admin/");
}

if(isset($_SESSION['login_applicant']) == 'login_applicant')
{
    header("location: applicant/");
}

if(isset($_SESSION['login_company']) == 'login_company')
{
    header("location: company/");
}

if(isset($_POST['login_button'])){
  
  $email_address = filter($_POST['email_address']);
  $user_pass = md5($_POST['user_pass']);

  $login = $dbcon->query("SELECT * FROM accounts WHERE email_address = '$email_address' AND user_pass='$user_pass' AND user_status = '1'") or die(mysqli_error());
  if(mysqli_num_rows($login) == 0){
    $msg = 'Wrong username/password or Your account is not yet verfied';
  }else{
    while($row = $login->fetch_assoc()){

      if($row['usertype'] == '0'){
          $_SESSION['user_id'] = $row['user_id'];
          $_SESSION['email_address'] = $row['email_address']; 
          $_SESSION['FirstName'] = $row['FirstName'];
          $_SESSION['LastName'] = $row['LastName'];
          $_SESSION['usertype'] = $row['usertype'];
          $_SESSION['login_admin'] = 'login_admin';

          header("location: admin/");

      }elseif($row['usertype'] == '1'){
          $_SESSION['user_id'] = $row['user_id']; 
          $_SESSION['email_address'] = $row['email_address']; 
          $_SESSION['FirstName'] = $row['FirstName'];
          $_SESSION['LastName'] = $row['LastName'];
          $_SESSION['usertype'] = $row['usertype'];
          $_SESSION['login_applicant'] = 'login_applicant';
          header("location: applicant/");
      }elseif($row['usertype'] == '2'){
          $_SESSION['user_id'] = $row['user_id']; 
          $_SESSION['FirstName'] = $row['FirstName'];
          $_SESSION['LastName'] = $row['LastName'];
          $_SESSION['usertype'] = $row['usertype'];
          $_SESSION['email_address'] = $row['email_address']; 
          $_SESSION['login_company'] = 'login_company';
        header("location: company/");
      }elseif($row['usertype'] == '3'){
        $_SESSION['user_id'] = $row['user_id']; 
        $_SESSION['FirstName'] = $row['FirstName'];
        $_SESSION['LastName'] = $row['LastName'];
        $_SESSION['usertype'] = $row['usertype'];
        $_SESSION['email_address'] = $row['email_address']; 
        $_SESSION['login_company'] = 'login_company';
        header("location: company/");
      }
    }

  }
}

 
?>
<?php include'dist/assets/header.php';?>
    <main role="main" style="">
      <div class="container marketing" style="margin-top:10%;">

        <!-- Three columns of text below the carousel -->
        
        <div >
          <div class="col-md-12" >
            <center><h1><i class="fa fa-lock"></i> Login Account</h1>
            <hr>
            <a href="" class="btn btn-primary"><i class="fa fa-facebook"></i> Login with Facebook</a>
            <br><br>
            <div class="container">
            <?php if(isset($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <form method="post">
              <div class="col-md-6 mx-sm-3">
                <input type="email" name="email_address" class="form-control" placeholder="Email Address" required="required">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="password" name="user_pass" class="form-control" placeholder="Password" required="required">
              </div>
              <p></p>

               <div class="col-md-6">
                <button class="btn btn-danger" name="login_button"><i class="fa fa-lock"></i> Login</button> <a href="">Forgot Password</a>
              </div>
              </form>
              <div class="col-md-6">
                New User? <a href="signup.php">Signup for free</a>
              </div>
              <p></p>
            </div>
            </center>
        </div>
</main>
      </div>

<?php include'dist/assets/footer.php';?>
<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}

if(isset($_POST['change_btn'])){
    $old_pass = md5($_POST['old_pass']);
    $new_pass = filter($_POST['new_pass']);
    $confirm_pass = filter($_POST['confirm_pass']);

    $g = getSingleRow("*","email_address","accounts",filter($_SESSION['email_address']));

    if($new_pass != $confirm_pass){
      $msg = 'Password do not matched.';
    }
    elseif($g['user_pass'] != $old_pass){
      $msg = 'Old password do no matched';
    }
    else{
    
    $arr_where = array("email_address"=>$_SESSION['email_address']);//update where
      $arr_set = array("user_pass"=>md5($new_pass));//set update
      $tbl_name = "accounts";
      $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);   
      $success = 'Password has been successfully updated.';
    }
  }
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-wrench"></i> Change Password</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php if(isset($msg)):?>
              <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $msg;?>
              <br />
            </div>
            <?php endif;?>
            <?php if(isset($success)):?>
              <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $success;?>
              <br />
            </div>
            <?php endif;?>
             <form method="post">
             <table class="table table-bordered">
                <tr>
                  <td>Old Password:</td>
                  <td><input type="password" name="old_pass" class="form-control" placeholder="Old Password" required></td>
                </tr>
                 <tr>
                  <td>New Password:</td>
                  <td><input type="password" name="new_pass" class="form-control" placeholder="New Password" required></td>
                </tr>
                 <tr>
                  <td>Confirm Password:</td>
                  <td><input type="password" name="confirm_pass" class="form-control" placeholder="Confirm Password" required></td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                     <button class="btn btn-primary btn-large" name="change_btn"><i class="fa fa-save"></i> Change Password</button>
                <a href="index.php" class="btn btn-danger btn-large"><i class="fa fa-arrow-left"></i> Return</a>
                  </td>
                </tr>
              </table>
          </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>
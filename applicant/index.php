<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$user = getSingleRow("*","user_id","applicant",filter($_SESSION['user_id']));
//Getting the Applicant personal information
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  <br><!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../dist/img/user4-128x128.jpg"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">
                  <?php echo $user['FirstName']?> <?php echo $user['MiddleName']?> <?php echo $user['LastName']?>
                </h3>

                <p class="text-muted text-center">Applicant</p>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fa fa-book mr-1"></i> Education</strong>

                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>

                <hr>

                <strong><i class="fa fa-map-marker mr-1"></i> Location</strong>

                <p class="text-muted">Malibu, California</p>

                <hr>

                <strong><i class="fa fa-pencil mr-1"></i> Skills</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">UI Design</span>
                  <span class="tag tag-success">Coding</span>
                  <span class="tag tag-info">Javascript</span>
                  <span class="tag tag-warning">PHP</span>
                  <span class="tag tag-primary">Node.js</span>
                </p>

                <hr>

                <strong><i class="fa fa-file-text-o mr-1"></i> Notes</strong>

                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <h4><i class="fa fa-user"></i> Personal Information</h4><hr>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                  <table class="table table-bordered">
                    <tr>
                      <td>Full Name:</td>
                      <td><?php echo $user['FirstName']?> <?php echo $user['MiddleName']?> <?php echo $user['LastName']?></td>
                    </tr>
                    <tr>
                      <td>Birthdate:</td>
                      <td><?php echo $user['birthdate']?></td>
                    </tr>
                    <tr>
                      <td>Contact Number:</td>
                      <td><?php echo $user['ContactNumber']?></td>
                    </tr>
                     <tr>
                      <td>Address:</td>
                      <td><?php echo $user['user_address']?></td>
                    </tr>
                    <tr>
                      <td>Email Address:</td>
                      <td><?php echo $_SESSION['email_address']?></td>
                    </tr>
                  </table>
                  <center>
                <a href="update-profile.php" class="btn btn-danger"><i class="fa fa-pencil"></i> Update Profile</a>
              </center>
                  <!--
                <h4><i class="fa fa-user"></i> Educational Background</h4><hr>
              -->

                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>
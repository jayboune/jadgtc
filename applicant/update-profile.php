<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$user = getSingleRow("*","user_id","applicant",filter($_SESSION['user_id']));
//Getting the Applicant personal information
if(isset($_POST['update_button'])){
  
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $email_address = filter($_POST['email_address']);
  $MiddleName = filter($_POST['MiddleName']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $birthdate = $_POST['birthdate'];
  $user_address = filter($_POST['user_address']);


  $arr_where = array("user_id"=>$_SESSION['user_id']);//update where
  $arr_set = array("MiddleName"=>$MiddleName,"ContactNumber"=>$ContactNumber,"birthdate"=>$birthdate, "email_address"=>$email_address,"user_address"=>$user_address);//set update
  $tbl_name = "applicant";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);   
  header("location: index.php");
  
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  <br><!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <h4><i class="fa fa-pencil"></i> Update Information</h4><hr>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
<?php if(isset($msg)):?>
  <div class="alert alert-danger"><?php echo $msg;?></div>
<?php endif;?>
<form method="post">
<div class="row">
  <div class="col-md-2">
    First Name:
  </div>
  <div class="col-md-10">
    <input type="text" name="FirstName" value="<?php echo $user['FirstName']?>" placeholder="First Name" class="form-control" readonly>
  </div>
  <br><br>
  <div class="col-md-2">
    Middle Name:
  </div>
  <div class="col-md-10">
    <input type="text" name="MiddleName" value="<?php echo $user['MiddleName']?>" placeholder="Middle Name" class="form-control" required>
  </div>
  <br><br>
  <div class="col-md-2">
    Last Name:
  </div>
  <div class="col-md-10">
    <input type="text" name="LastName" value="<?php echo $user['LastName']?>" placeholder="Last Name" class="form-control" readonly>
  </div>
  <br><br>
  <div class="col-md-2">
    Birthdate:
  </div>
  <div class="col-md-10">
    <input type="date" name="birthdate" value="<?php echo $user['birthdate']?>" class="form-control">
  </div>
  <br><br>
  <div class="col-md-2">
    Contact Number:
  </div>
  <div class="col-md-10">
    <input type="text" name="ContactNumber" value="<?php echo $user['ContactNumber']?>" class="form-control" required placeholder="Contact Number">
  </div>
  <br><br>
  <div class="col-md-2">
    Address:
  </div>
  <div class="col-md-10">
    <input type="text" name="user_address" value="<?php echo $user['user_address']?>" class="form-control" required placeholder="Address">
  </div>
  <br><br>
   <div class="col-md-2">
    Email Address:
  </div>
  <div class="col-md-10">
    <input type="text" name="email_address" value="<?php echo $user['email_address']?>" class="form-control" required placeholder="Email Address">
  </div>
  <br><br>
</div>
<center>
  <button class="btn btn-primary" name="update_button"><i class="fa fa-save"></i> Save</button>
  <a href="index.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Return</a>
</center>
</form>
                  <!--
                <h4><i class="fa fa-user"></i> Educational Background</h4><hr>
              -->

                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>
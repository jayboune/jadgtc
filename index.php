<?php 
include'config/db.php';
if(isset($_SESSION['login_admin']) == 'login_admin')
{
    header("location: admin/");
}

if(isset($_SESSION['login_applicant']) == 'login_applicant')
{
    header("location: applicant/");
}

if(isset($_SESSION['login_company']) == 'login_company')
{
    header("location: company/");
}
?>
<?php include'dist/assets/header.php';?>
    <main role="main">

      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="img/website_banner04.jpg" alt="First slide">
            <div class="container">

              <div class="carousel-caption">
                
                <h1 style="font-size:45px;">Welcome to JAD + GTC Manpower Supply & Services Inc.</h1>
                <h4>Your one-stop-shop service provider of professional and skilled <strong>world-class Filipino manpower</strong></h4>
                <p>
                  <a class="btn btn-lg btn-primary" href="" role="button"><i class="fa fa-users"></i> Job Seekers</a>
                  <a class="btn btn-lg btn-warning" href="" role="button"><i class="fa fa-plus"></i> Post a Job</a>
                </p>
              
              </div>
            </div>
          </div>
          
          <div class="carousel-item">
            <img class="second-slide" src="img/website_banner05.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <h1>Another example headline.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="https://getbootstrap.com/docs/4.1/examples/carousel/#" role="button">Learn more</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="third-slide" src="img/website_banner06.jpg" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>One more for good measure.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="https://getbootstrap.com/docs/4.1/examples/carousel/#" role="button">Browse gallery</a></p>
              </div>
            </div>
          </div>
        
        </div>
        
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>


      <!-- Marketing messaging and featurettes
      ================================================== -->
      <!-- Wrap the rest of the page in another container to center all the content. -->

      <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        <div class="row" id="#about">
         
          <div class="col-lg-6">
            <center>
            <img class="rounded-circle" src="img/img_mis.png" alt="Generic placeholder image" width="140" height="140">
            <h2>Mission</h2>
          </center>
            <p class="lead">Our mission is to exceed our Client’s expectations by delivering quality manpower services through-out the project’s life cycle. It is also our desire to promote world class expertise and culture of Filipino workforce.</p>
            
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-6">
            <center>
            <img class="rounded-circle" src="img/img_mis.png" alt="Generic placeholder image" width="140" height="140">
            <h2>Vision</h2>
          </center>
            <p class="lead">To be a premiere and diversified provider of professional, technical, and skilled Filipino manpower  for Transportation, Healthcare, Oil & Gas, and  Industrial sector globally.</p>
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
       <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-12 order-md-2">
            <center><h1>ABOUT US</h1></center>
            <p class="lead">JAD+GTC aims to provide the best HR solution based on the client’s needs, and an access to a qualified talent supply that
provides the proper mix of skills, experience, and knowledge to fit unique needs.</p>
<p class="lead">
JAD+GTC aims to empower their employees and maximize their potential in the recruitment field, acknowledging that 
the success of the company relies on the competence and passion of its people. </p>
<p class="lead">
JAD+GTC aims to be a catalyst of the actualization of our applicants’ dreams, by directing them to jobs that is most suitable for 
them, and supporting them not only throughout the length of the recruitment process but as they transition in their new work place. </p>
          </div>
         
        </div>
</main>
        

        
        

        <!-- /END THE FEATURETTES -->

      </div>
      <div style="background:#303f9f;color:white;">
          <div class="col-md-12">
            <div class="container">
            <center><h1>CONTACT US</h1>
            <h4>2F, 2310 Don Chino Roces Ave (formerly Pasong Tamo, 
across Makati Cinema Square) 1230, Makati, Metro Manila</h4>
<h4>Tel. No (824-64.47) / Fax No. (812.97.63)</h4>
<h4>Email: humanresource@jadgtc.net</h4>
<h4>Facebook: jadgtcmanpower</h4>
          </center>
            <form method="post">
              <div style="width: 50%; margin: 0 auto;">
                <div class="col-md-12">
                  <input type="text" name="name" placeholder="Name" class="form-control">
                </div>
                <p></p>
                <div class="col-md-12">
                  <input type="email" name="email_address" placeholder="Email Address" class="form-control">
                </div>
                <p></p>
                 <div class="col-md-12">
                  <input type="text" name="message" placeholder="Message" class="form-control">
                </div>
                <p></p>
                <div class="col-md-12">
                <center>
                  <button class="btn btn-success btn-lg"><i class="fa fa-send"></i> Submit</button>
                </center>
                </div>
              </div>
              <br>
              <div class="row">
               
              </div>
              <br>
            </form>
          </div>
        </div>
        </div>
<?php include'dist/assets/footer.php';?>
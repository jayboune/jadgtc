<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$company = fetchAll("*","companyclient"); 
// SELECT all data from the accounts table where usertype =1

if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
  $user_id = filter($_GET['user_id']);
  $ar = array("clientID"=>$delete); //WHERE statement
  $tbl_name = "companyclient"; 
  $del = delete($dbcon,$tbl_name,$ar);
  if($del){
    $ar2 = array("user_id"=>$delete); //WHERE statement
    $tbl_name2 = "accounts"; 
    $del = delete($dbcon,$tbl_name2,$ar2
    );
    header("location: company.php");
  }
}
if(isset($_GET['verify'])){

  $arr_where = array("user_id"=>$_GET['verify']);//update where
  $arr_set = array("user_status"=>"1");//set update
  $tbl_name = "accounts";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);
  header("location: company.php");
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-home"></i> Company Partners</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <?php  if(!empty($company)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Company Name</th>
                  <th>Email Address</th>
                  <th>Company Address</th>
                  <th>Contact Number</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($company as $key => $value):?>
                <tr>
                  <td><?php echo $value->CompanyName?></td>
                  <td><?php echo $value->email_address?></td>
                  <td><?php echo $value->client_address?></td>
                  <td><?php echo $value->ContactNumber?></td>
                  <td>
                    <?php $status = getSingleRow("*","user_id","accounts",$value->user_id);?>
                    <?php if($status['user_status'] == '0'):?>Unverfied<?php else:?>Verfied<?php endif;?>
                  </td>
                  <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-info">View</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
<ul class="dropdown-menu" role="menu">
  <?php if($status['user_status'] == '0'):?>
  <li style="margin:5px;"><a href="#" <?php echo 'onclick=" confirm(\'Are you sure you want to Verify?\') 
      ?window.location = \'company.php?verify='.$value->user_id.'\' : \'\';"'; ?>>Verify Account</a></li>
  <?php else:?>
  <li style="margin:5px;"><a href="#" <?php echo 'onclick=" confirm(\'Are you sure you want to delete?\') 
      ?window.location = \'company.php?delete='.$value->clientID.'&user_id='.$value->user_id.'\' : \'\';"'; ?>>Delete</a></li>
  <?php endif;?>         
</ul>
        </div>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database</div>
              <?php endif;?>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>
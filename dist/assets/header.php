<!DOCTYPE html>
<!-- saved from url=(0052)https://getbootstrap.com/docs/4.1/examples/carousel/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>JAD + GTC Manpower Supply & Services</title>

    <!-- Bootstrap core CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  </head>
  <body>

    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href=""><a class="nav-link" href=""><img src="img/jadgtc_logo.png" height="100%" width="100%"></a></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="background:black;">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            </li>
          </ul>

          
          <ul class="navbar-nav form-inline mt-2 mt-md-0">
            <li class="nav-item">
              <a href="index.php" class="nav-link" style="color:black; padding:20px;"><i class="fa fa-home"></i> HOME</a>
            </li>
            
            <li class="nav-item">
              <a href="" class="nav-link" style="color:black; padding:20px;"><i class="fa fa-th"></i> EMPLOYERS</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:black; padding:20px;"><i class="fa fa-users"></i> 
          SIGNUP
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="signup.php">Signup as Applicant</a>
          <a class="dropdown-item" href="company.php">Signup as Employer</a>
             <li class="nav-item">
              <a href="login.php" class="nav-link" style="color:black; padding:20px;"><i class="fa fa-lock"></i> LOGIN</a>
            </li>     
            </li>
          </ul>
        </div>
      </nav>
    </header>